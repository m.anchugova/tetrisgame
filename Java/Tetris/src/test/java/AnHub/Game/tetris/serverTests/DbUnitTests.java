package AnHub.Game.tetris.serverTests;

import org.dbunit.DBTestCase;
import org.dbunit.PropertiesBasedJdbcDatabaseTester;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;

public abstract class DbUnitTests extends DBTestCase {
    private String host;
    private String login;
    private String password;

    public DbUnitTests(String name) {
        super(name);
        initializeDataBaseSettings();
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS, "com.mysql.jdbc.Driver");
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL, "jdbc:mysql://" + host +":3306/tetris");
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME, login);
        System.setProperty(PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD, password);
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
        return null;
    }


    protected DatabaseOperation getSetUpOperation() {
        return DatabaseOperation.CLEAN_INSERT;
    }

    protected DatabaseOperation getTearDownOperation() {
        return DatabaseOperation.NONE;
    }

    private void initializeDataBaseSettings(){
        login = "root";
        host = System.getenv("MYSQL_HOST");
        password = System.getenv("MYSQL_PASSWORD");
    }
}
