package AnHub.Game.tetris.maingameTests;

import anhub.game.tetris.maingame.eColor;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EColorTest {
    @Test
    public void blackTest(){
        eColor color = eColor.BLACK;
        assertEquals("black", eColor.getStringColor(color));
    }

    @Test
    public void redTest(){
        eColor color = eColor.RED;
        assertEquals("red", eColor.getStringColor(color));
    }

    @Test
    public void greenTest(){
        eColor color = eColor.GREEN;
        assertEquals("green", eColor.getStringColor(color));
    }

    @Test
    public void blueTest(){
        eColor color = eColor.BLUE;
        assertEquals("blue", eColor.getStringColor(color));
    }

    @Test
    public void aquaTest(){
        eColor color = eColor.AQUA;
        assertEquals("aqua", eColor.getStringColor(color));
    }

    @Test
    public void yellowTest(){
        eColor color = eColor.YELLOW;
        assertEquals("yellow", eColor.getStringColor(color));
    }

    @Test
    public void orangeTest(){
        eColor color = eColor.ORANGE;
        assertEquals("orange", eColor.getStringColor(color));
    }

    @Test
    public void purpleTest(){
        eColor color = eColor.PURPLE;
        assertEquals("purple", eColor.getStringColor(color));
    }
}
