package AnHub.Game.tetris.maingameTests;

import anhub.game.tetris.maingame.ShiftDirection;
import org.junit.Test;

import static org.junit.Assert.*;

public class ShiftDirectionTest {
    @Test
    public void leftDirectionTest(){
        String direction = "LEFT";
        assertEquals(ShiftDirection.LEFT, ShiftDirection.getShiftDirection(direction));
    }

    @Test
    public void rightDirectionTest(){
        String direction = "RIGHT";
        assertEquals(ShiftDirection.RIGHT, ShiftDirection.getShiftDirection(direction));
    }

    @Test
    public void awaitingDirectionTest(){
        String direction = "AWAITING";
        assertEquals(ShiftDirection.AWAITING, ShiftDirection.getShiftDirection(direction));
    }

    @Test
    public void errorDirectionTest(){
        String direction = "UP";
        try {
            ShiftDirection shiftDirection = ShiftDirection.getShiftDirection(direction);
            assertNull(shiftDirection);
        }
        catch (IllegalStateException e){
            assertNotEquals("", e.getMessage());
        }
    }
}
