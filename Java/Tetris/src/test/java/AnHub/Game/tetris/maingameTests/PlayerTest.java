package AnHub.Game.tetris.maingameTests;

import anhub.game.tetris.maingame.Player;
import org.junit.Test;

import static org.junit.Assert.*;

public class PlayerTest {
    @Test
    public void idTest() {
        Player player = new Player("1");
        assertEquals("1", player.getId());
    }

    @Test
    public void scoreTest() {
        Player player = new Player("1");
        player.addScore();
        assertEquals(10, player.getScore());
    }
}
