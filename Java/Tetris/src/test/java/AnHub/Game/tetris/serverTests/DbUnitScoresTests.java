package AnHub.Game.tetris.serverTests;

import anhub.game.tetris.controller.GameController;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Test;

import java.io.FileInputStream;

import static org.dbunit.Assertion.assertEqualsIgnoreCols;

public class DbUnitScoresTests extends DbUnitTests {

    public DbUnitScoresTests(String name) {
        super(name);
    }

    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/AnHub/Game/tetris/serverTests/scores.xml"));
    }

    @Test
    public void testInsertNewScore() throws Exception {
        GameController gameController = new GameController();
        gameController.insertScore("50", 300);

        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("scores");

        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/AnHub/Game/tetris/serverTests/scores_insert.xml"));

        ITable expectedTable = expectedDataSet.getTable("scores");

        assertEqualsIgnoreCols(expectedTable, actualTable, new String[] { "idScores", "date" });
    }
}


