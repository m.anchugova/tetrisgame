package AnHub.Game.tetris.maingameTests;

import anhub.game.tetris.maingame.*;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class FigureTest {
    private GameField gameField;

    @Before
    public void setUp() {
        gameField = new GameField();
    }

    @Test
    public void ShiftFigureLeftTest(){
        Coord coord = gameField.getFigure().getMetaPointCoords();
        int x = coord.getX();
        int y = coord.getY();

        boolean wasShift = gameField.tryShiftFigure(ShiftDirection.LEFT);

        if(wasShift){
            assertEquals(coord.getX(), x - 1);
        }
        else{
            assertEquals(coord.getX(), x);
        }

        assertEquals(coord.getY(), y);
    }

    @Test
    public void ShiftFigureRightTest(){
        Coord coord = gameField.getFigure().getMetaPointCoords();
        int x = coord.getX();
        int y = coord.getY();

        boolean wasShift = gameField.tryShiftFigure(ShiftDirection.RIGHT);

        if(wasShift){
            assertEquals(coord.getX(), x + 1);
        }
        else{
            assertEquals(coord.getX(), x);
        }

        assertEquals(coord.getY(), y);
    }

    @Test
    public void RotateFigureTest(){
        RotationMode rotationMode = gameField.getFigure().getCurrentRotation();
        RotationMode nextRotationMode = RotationMode.getNextRotationFrom(rotationMode);

        boolean wasRotate = gameField.tryRotateFigure();
        RotationMode newRotationMode = gameField.getFigure().getCurrentRotation();

        if(wasRotate){
            assertEquals(newRotationMode, nextRotationMode);
        }
        else{
            assertEquals(newRotationMode, rotationMode);
        }
    }

    @Test
    public void FallFigureTest(){
        eColor colorFigure = null;
        Coord[] coords = null;

        while(true) {
            Coord metaPointCoord = gameField.getFigure().getMetaPointCoords();
            int y = metaPointCoord.getY();
            Boolean[] a=new Boolean[1];
            if(gameField.letFallDown(a)) {
                assertEquals(metaPointCoord.getY(), y - 1);
                colorFigure = gameField.getFigure().getColor();
                coords = gameField.getFigure().getCoords();
            }
            else {
                break;
            }
        }

        eColor[][] theField = gameField.getTheField();

        assertNotNull(coords);
        for (Coord coord : coords) {
            assertEquals(theField[coord.getX()][coord.getY()], colorFigure);
        }
    }
}
