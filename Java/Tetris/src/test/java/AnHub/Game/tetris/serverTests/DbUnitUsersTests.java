package AnHub.Game.tetris.serverTests;

import java.io.FileInputStream;

import anhub.game.tetris.controller.Authorization;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Test;

import static org.dbunit.Assertion.assertEqualsIgnoreCols;

public class DbUnitUsersTests extends DbUnitTests {

    public DbUnitUsersTests(String name) {
        super(name);
    }

    protected IDataSet getDataSet() throws Exception {
        return new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/AnHub/Game/tetris/serverTests/users.xml"));
    }

    @Test
    public void testInsertNewUser() throws Exception {
        Authorization authorization = new Authorization();
        authorization.insertNewUser("user100", "password100");

        IDataSet databaseDataSet = getConnection().createDataSet();
        ITable actualTable = databaseDataSet.getTable("users");

        IDataSet expectedDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("src/test/java/AnHub/Game/tetris/serverTests/users_insert.xml"));

        ITable expectedTable = expectedDataSet.getTable("users");

        assertEqualsIgnoreCols(expectedTable, actualTable, new String[] { "id" });
    }
}

