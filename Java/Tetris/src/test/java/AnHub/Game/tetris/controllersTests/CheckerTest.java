package AnHub.Game.tetris.controllersTests;

import anhub.game.tetris.controller.Authorization;
import anhub.game.tetris.controller.Checker;
import anhub.game.tetris.server.OracleJDBCEx;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.Random;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CheckerTest {
    private Checker checker;
    private OracleJDBCEx bdConnect= OracleJDBCEx.getInstance();

    @Before
    public void setUp() {
        checker = new Checker();
    }

    @Test
    public void checkSuccessTest() {
        String createUserMessage = "checkerTest+111=";
        Authorization authorization = new Authorization();
        authorization.list(createUserMessage);

        String id = bdConnect.getSqlQuery("Select id , password FROM users WHERE login = 'checkerTest'")[0];

        Map<String, Boolean> dictionary = checker.list(id + "=");
        assertTrue(dictionary.get("success"));

        bdConnect.sqlUpdate("DELETE FROM users WHERE login='checkerTest'");
    }

    @Test
    public void checkFailTest() {
        String id;
        Random random = new Random();
        while(true) {
            int checkId = random.nextInt();
            String[] res = bdConnect.getSqlQuery("SELECT count(id) ,login FROM users WHERE id = " + checkId);
            if(res[0].equals("0")){
                id = res[0];
                break;
            }
        }

        Map<String, Boolean> dictionary = checker.list(id + "=");
        assertFalse(dictionary.get("success"));
    }
}
