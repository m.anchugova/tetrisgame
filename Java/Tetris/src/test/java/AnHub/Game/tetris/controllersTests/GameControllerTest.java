package AnHub.Game.tetris.controllersTests;

import anhub.game.tetris.controller.GameController;
import org.junit.Test;
import org.junit.Before;
import static org.junit.Assert.*;

import java.util.Map;

import static anhub.game.tetris.maingame.Constants.*;

public class GameControllerTest {
    private GameController gameController;

    @Before
    public void setUp() {
        gameController = new GameController();
    }

    @Test
    public void startGameTest() {
        String message = "start=";
        Map<String, String[][]> dictionary = gameController.list(message);
        String[][] req = dictionary.get("field");
        for (int x = 0; x < COUNT_CELLS_X; x++) {
            for (int y = 0; y < COUNT_CELLS_Y; y++) {
                assertEquals("black", req[x][y]);
            }
        }
    }

    @Test
    public void uselessPostMappingTest(){
        String message="";
        Map<String, String[][]> dictionary = gameController.list(message);
        assertEquals(0, dictionary.size());
    }

    @Test
    public void getMappingTest(){
        String message = gameController.glist();
        assertEquals("Ухади", message);
    }
}
