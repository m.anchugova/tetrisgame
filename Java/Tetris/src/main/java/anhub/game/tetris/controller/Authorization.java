package anhub.game.tetris.controller;

import anhub.game.tetris.server.OracleJDBCEx;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("log")
public class Authorization {

    private OracleJDBCEx bdConnect= OracleJDBCEx.getInstance();
    @CrossOrigin
    @PostMapping
    public Map<String,String> list(@RequestBody String message) {

        Map<String, String> dictionary = new HashMap<>();
        String[] data = message.split("\\+",0);
        String login= data[0];
        String password=data[1].substring(0,data[1].length()-1);
        String passFromDb;
        String id;

        String[] res =  getPasswordAndIdByLogin(login);

        if(res.length == 0) {
            insertNewUser(login,password);
            passFromDb=password;
            id= getLastInsertIdUser();
        }
        else {
            passFromDb=res[0];
            id=res[1];
        }

        assert passFromDb != null;
        if(passFromDb.equals(password)){
            dictionary.put("site","index.html");

        }else {
            dictionary.put("site","Error.html");

        }
        dictionary.put("id",id);
        return dictionary;



    }

    public void insertNewUser(String login, String password){
        bdConnect.sqlUpdate("insert into users(id,login,password) values (default, '"+login+"' , '"+password+"')");
    }

    public String getLastInsertIdUser(){
        String[] res= bdConnect.getSqlQuery("select LAST_INSERT_ID(), id from users");
        return res[0];
    }

    public String[] getPasswordAndIdByLogin(String login){
        return bdConnect.getSqlQuery("select password, id from users where login='"+login+"'");
    }
}
