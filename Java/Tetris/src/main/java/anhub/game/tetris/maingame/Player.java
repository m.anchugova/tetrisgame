package anhub.game.tetris.maingame;

public class Player {
    public String getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    String id;
    int score = 0;

    public Player(String id){
        this.id=id;
    }
    public void addScore(){
        score+=10;
    }

}
