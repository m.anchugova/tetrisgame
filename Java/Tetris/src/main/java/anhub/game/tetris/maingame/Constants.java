package anhub.game.tetris.maingame;

public class  Constants{

    /* Количество ячеек на экране по горизонтали и вертикали */
    public static final int COUNT_CELLS_X = 10;
    public static final int COUNT_CELLS_Y = 2 * COUNT_CELLS_X;

    /*Невидимое пространство сверху, в котором создаются фигуры*/
    public static final int OFFSET_TOP = 3;

    /* Цвет, которым в поле обозначается пустота */
    public static final eColor EMPTINESS_COLOR = eColor.BLACK;

    /* Максимальная  ширина фигуры */
    public static final int MAX_FIGURE_WIDTH = 4;

    private Constants() {

    }
}

