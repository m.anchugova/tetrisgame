package anhub.game.tetris.controller;

import anhub.game.tetris.server.OracleJDBCEx;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("check")
public class Checker {
    private OracleJDBCEx bdConnect= OracleJDBCEx.getInstance();

    @CrossOrigin
    @PostMapping
    public Map<String,Boolean> list(@RequestBody String message) {
        message=message.substring(0,message.length()-1);
        Map<String, Boolean> dictionary = new HashMap<>();
        boolean success=false;
        String[] res =bdConnect.getSqlQuery("Select login, password from users where id="+message);
        if(res.length != 0){
            success=true;
        }
        dictionary.put("success",success);
        return dictionary;
    }
}
