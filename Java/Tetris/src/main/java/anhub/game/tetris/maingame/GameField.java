package anhub.game.tetris.maingame;

import java.security.SecureRandom;

public class GameField {
    private eColor[][] theField;
    private int[] countFilledCellsInLine;
    private Figure figure;
    private SecureRandom random = new SecureRandom();

    public GameField(){
        spawnNewFigure();

        theField = new eColor[Constants.COUNT_CELLS_X][Constants.COUNT_CELLS_Y+ Constants.OFFSET_TOP];
        countFilledCellsInLine = new int[Constants.COUNT_CELLS_Y+ Constants.OFFSET_TOP];
        for(int y =0;y < Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP; y++){
                for(int x = 0; x < Constants.COUNT_CELLS_X; x++){
                    theField[x][y] = Constants.EMPTINESS_COLOR;
                }
            }
        }
    private void spawnNewFigure(){
                int randomX = random.nextInt(Constants.COUNT_CELLS_X - Constants.MAX_FIGURE_WIDTH);

                this.figure = new Figure(new Coord(randomX, Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP - 1));
            }
    public boolean isEmpty(int x, int y){
        return (theField[x][y].equals(Constants.EMPTINESS_COLOR));
    }
    public eColor getColor(int x, int y) {
        return theField[x][y];
    }
    public Figure getFigure() {
        return figure;
    }
    public boolean tryShiftFigure(ShiftDirection shiftDirection) {
        Coord[] shiftedCoords = figure.getShiftedCoords(shiftDirection);

        boolean canShift = canDo(shiftedCoords);

        if(canShift){
            figure.shift(shiftDirection);
        }

        return canShift;
    }
    public boolean tryRotateFigure() {
        Coord[] rotatedCoords = figure.getRotatedCoords();

        boolean canRotate = canDo(rotatedCoords);

        if(canRotate){
            figure.rotate();
        }

        return canRotate;
    }
    public boolean letFallDown(Boolean[] flagShiftDown) {
        Coord[] fallenCoords = figure.getFallenCoords();

        boolean canFall = canDo(fallenCoords);

        if(canFall) {
            figure.fall();
        } else {
            Coord[] figureCoords = figure.getCoords();

            /* Флаг, говорящий о том, что после будет необходимо сместить линии вниз
             * (т.е. какая-то линия была уничтожена)
             */
            boolean haveToShiftLinesDown = false;

            for(Coord coord: figureCoords) {
                int x = coord.getX();
                int y = coord.getY();

                theField[x][y] = figure.getColor();

                /* Увеличиваем информацию о количестве статичных блоков в линии*/
                countFilledCellsInLine[y]++;

                /* Проверяем, полностью ли заполнена строка Y
                 * Если заполнена полностью, устанавливаем  haveToShiftLinesDown в true
                 */
                haveToShiftLinesDown = tryDestroyLine(y) || haveToShiftLinesDown;
            }
            flagShiftDown[flagShiftDown.length-1]=haveToShiftLinesDown;
            /* Если это необходимо, смещаем линии на образовавшееся пустое место */
            if(haveToShiftLinesDown) shiftLinesDown();

            /* Создаём новую фигуру взамен той, которую мы перенесли*/
            spawnNewFigure();
        }

        return canFall;
    }
    private void shiftLinesDown() {

        /* Номер обнаруженной пустой линии (-1, если не обнаружена) */
        int fallTo = -1;

        /* Проверяем линии снизу вверх*/
        for(int y = 0; y < Constants.COUNT_CELLS_Y; y++){
            if(fallTo == -1){ //Если пустот ещё не обнаружено
                if(countFilledCellsInLine[y] == 0) fallTo = y; //...пытаемся обнаружить (._.)
            }else { //А если обнаружено
                if(countFilledCellsInLine[y] != 0){ // И текущую линию есть смысл сдвигать...

                    /* Сдвигаем... */
                    for(int x = 0; x < Constants.COUNT_CELLS_X; x++){
                        theField[x][fallTo] = theField[x][y];
                        theField[x][y] = Constants.EMPTINESS_COLOR;
                    }

                    /* Не забываем обновить мета-информацию*/
                    countFilledCellsInLine[fallTo] = countFilledCellsInLine[y];
                    countFilledCellsInLine[y] = 0;

                    /*
                     * В любом случае линия сверху от предыдущей пустоты пустая.
                     * Если раньше она не была пустой, то сейчас мы её сместили вниз.
                     * Если раньше она была пустой, то и сейчас пустая -- мы её ещё не заполняли.
                     */
                    fallTo++;
                }
            }
        }
    }
    private boolean tryDestroyLine(int y) {
        if(countFilledCellsInLine[y] < Constants.COUNT_CELLS_X){
            return false;
        }

        for(int x = 0; x < Constants.COUNT_CELLS_X; x++){
            theField[x][y] = Constants.EMPTINESS_COLOR;
        }

        /* Не забываем обновить мета-информацию! */
        countFilledCellsInLine[y] = 0;

        return true;
    }
    public boolean isOverfilled(){
        boolean ret = false;

        for(int i = 0; i < Constants.OFFSET_TOP; i++){
            if(countFilledCellsInLine[Constants.COUNT_CELLS_Y+i] != 0) ret = true;
        }

        return ret;
    }

    private boolean canDo(Coord[] coords){
        for(Coord coord: coords) {
            int x = coord.getX();
            int y = coord.getY();
            if((y<0 || y>= Constants.COUNT_CELLS_Y + Constants.OFFSET_TOP)
                    || (x<0 || x>= Constants.COUNT_CELLS_X)
                    || !isEmpty(x, y)){
                return false;
            }
        }

        return true;
    }

    public eColor[][] getTheField(){
        return theField;
    }
}

