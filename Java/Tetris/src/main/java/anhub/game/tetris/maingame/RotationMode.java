package anhub.game.tetris.maingame;

public enum RotationMode {
    NORMAL(0),
    FLIP_ONES(1),
    INVERT(2),
    FLIP_TWICE(3);
    private int number;
    RotationMode(int number){
        this.number = number;
    }
    private static RotationMode[] rotationByNumber = {NORMAL, FLIP_ONES, INVERT, FLIP_TWICE};
    public static RotationMode getNextRotationFrom(RotationMode perviousRotation) {
        int newRotationIndex = (perviousRotation.number + 1) % rotationByNumber.length;
        return rotationByNumber[newRotationIndex];
    }
}
