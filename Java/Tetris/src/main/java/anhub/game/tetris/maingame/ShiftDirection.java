package anhub.game.tetris.maingame;

public enum  ShiftDirection {
    AWAITING,
    LEFT,
    RIGHT;

    public static ShiftDirection getShiftDirection(String direction){
        switch (direction) {
            case "LEFT":
                return LEFT;
            case "RIGHT":
                return RIGHT;
            case "AWAITING":
                return AWAITING;
            default:
                throw new IllegalStateException("Unexpected value: " + direction);
        }
    }
}
