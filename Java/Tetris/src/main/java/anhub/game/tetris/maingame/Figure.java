package anhub.game.tetris.maingame;

public class Figure {
    private Coord metaPointCoords;
    private RotationMode currentRotation;
    private FigureForm form;
    public Figure(Coord metaPointCoords){
        this(metaPointCoords, RotationMode.NORMAL, FigureForm.getRandomForm());
    }
    public Figure(Coord metaPointCoords, RotationMode rotation, FigureForm form){
        this.metaPointCoords = metaPointCoords;
        this.currentRotation = rotation;
        this.form = form;
    }
    public Coord[] getCoords(){
        return form.getMask().generateFigure(metaPointCoords, currentRotation);
    }
    public Coord[] getRotatedCoords(){
        return form.getMask().generateFigure(metaPointCoords, RotationMode.getNextRotationFrom(currentRotation));
    }
    public void rotate(){
        this.currentRotation = RotationMode.getNextRotationFrom(currentRotation);
    }
    public Coord[] getShiftedCoords(ShiftDirection direction){
        Coord newFirstCell = null;

        switch (direction){
            case LEFT:
                newFirstCell = new Coord(metaPointCoords.getX() - 1, metaPointCoords.getY());
                break;
            case RIGHT:
                newFirstCell = new Coord(metaPointCoords.getX() + 1, metaPointCoords.getY());
                break;
            case AWAITING:
                break;
        }

        return form.getMask().generateFigure(newFirstCell, currentRotation);
    }

    public void shift(ShiftDirection direction){
        switch (direction){
            case LEFT:
                metaPointCoords.setX(metaPointCoords.getX() - 1);
                break;
            case RIGHT:
                metaPointCoords.setX(metaPointCoords.getX() + 1);
                break;
            case AWAITING:
                break;
        }
    }
    public Coord[] getFallenCoords(){
        Coord newFirstCell = new Coord(metaPointCoords.getX(), metaPointCoords.getY() - 1);

        return form.getMask().generateFigure(newFirstCell, currentRotation);
    }
    public void fall(){
        metaPointCoords.setY(metaPointCoords.getY() - 1);
    }
    public eColor getColor() {
        return form.getColor();
    }

    public Coord getMetaPointCoords(){
        return metaPointCoords;
    }
    public RotationMode getCurrentRotation(){
        return currentRotation;
    }
}