package anhub.game.tetris.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class StartJs {

    @GetMapping(value = "/start")
    public String welcome() {
        return "index.html";
    }
}
