package anhub.game.tetris.controller;

import anhub.game.tetris.maingame.*;
import anhub.game.tetris.server.OracleJDBCEx;
import org.apache.logging.log4j.*;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import static anhub.game.tetris.maingame.Constants.*;

@RestController
@RequestMapping("step")
public class GameController {
    Main m=new Main();
    boolean start =false;
    private static final Logger logger = LogManager.getLogger();
    Player p;
    AtomicBoolean insert=new AtomicBoolean(false);
    private OracleJDBCEx bdConnect= OracleJDBCEx.getInstance();
    @CrossOrigin
    @PostMapping

    public Map<String,String[][]> list(@RequestBody String message){
        logger.info(message);

        message = checkStartMessage(message);

        logger.info(m.isEndOfGame());

        if (start) {
            return startGame(message);
        }

        return new HashMap<>();
    }
    @CrossOrigin
    @GetMapping
    public String glist(){
        return "Ухади";
    }

    private String checkStartMessage(String message){
        if(message.equals("start=")){
            start=true;
            message="direction=AWAITING&was_boost=false&was_rotation=false&was_esc=false";
            p= new Player("2");
        }

        return message;
    }

    private Map<String,String[][]> startGame(String message){
        String[] states = message.split("&");
        ShiftDirection sd = ShiftDirection.getShiftDirection(states[0].substring(states[0].indexOf('=')+1));
        logger.info(sd);

        boolean wasRoot = Boolean.parseBoolean(states[2].substring(states[2].indexOf('=')+1));
        boolean wasEsc = Boolean.parseBoolean(states[3].substring(states[3].indexOf('=')+1));

        m.input(sd, wasRoot, wasEsc);
        int resLogic=m.logic();
        switch (resLogic){
            case 1:
                p.addScore();
                return new HashMap<>();
            case 0:
                return getField();
            default:
                start = false;
                return getScore();
        }
    }

    private Map<String,String[][]> getScore(){
        Map<String,String[][]> dictionary = new HashMap<>();

        if(!insert.get())
        {
            insertScore(p.getId(), p.getScore());
            insert.set(true);
        }

        String[] res = bdConnect.getSqlQuery("" +
                "SELECT date, score from scores where idUser="+p.getId()
        );
        String[][] score = new String[res.length][res.length];  
        int k=0;
        for (int i=0;i<res.length;i+=2){

            score[k][0]=res[i];
            if(i+1>res.length){
                break;
            }
            score[k][1]=res[i+1];
            k++;

        }
        dictionary.put("SCORE",score);
        dictionary.put("redirect", null);

        return dictionary;
    }

    private Map<String,String[][]> getField(){
        Map<String,String[][]> dictionary = new HashMap<>();

        Figure figure = m.getFigure();
        String[][] req = new String[COUNT_CELLS_X][COUNT_CELLS_Y];
        String[][] fig = new String[COUNT_CELLS_X][COUNT_CELLS_Y+OFFSET_TOP];

        eColor figColor = figure.getColor();
        for(Coord coord : figure.getCoords()){
            fig[coord.getX()][coord.getY()] = eColor.getStringColor(figColor);
        }
        for (int x = 0; x < COUNT_CELLS_X; x++) {
            for (int y = 0; y < COUNT_CELLS_Y; y++) {
                eColor color = m.getColor(x, y);
                req[x][y] = eColor.getStringColor(color);
            }
        }
        dictionary.put("field",req);
        dictionary.put("figure",fig);

        return dictionary;
    }

    public void insertScore(String id, int score){
        bdConnect.sqlUpdate("" +
                "INSERT INTO scores (idScores,date,idUser,score)\n" +
                "VALUES(DEFAULT,NOW(),"+id+","+score+")");
    }
}
