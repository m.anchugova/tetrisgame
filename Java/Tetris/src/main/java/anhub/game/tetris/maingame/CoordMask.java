package anhub.game.tetris.maingame;

public enum CoordMask {
    I(
            (initialCoord, rotation) -> {
                Coord[] ret = new Coord[4];

                int x = initialCoord.getX();
                int y = initialCoord.getY();

                switch (rotation){
                    case NORMAL:
                    case INVERT:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x, y - 1);
                        ret[2] = createCoord(x,y - 2);
                        ret[3] = createCoord(x, y - 3);
                        break;
                    case FLIP_TWICE:
                    case FLIP_ONES:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x + 1, y);
                        ret[2] = createCoord(x + 2, y);
                        ret[3] = createCoord(x + 3, y);
                        break;

                }

                return ret;
            }
    ),
    J(
            (initialCoord, rotation) -> {
                Coord[] ret = new Coord[4];

                int x = initialCoord.getX();
                int y = initialCoord.getY();

                switch (rotation){
                    case NORMAL:
                        ret[0] = createCoord(x + 1 , y);
                        ret[1] = createCoord(x + 1, y - 1);
                        ret[2] = createCoord(x + 1, y - 2);
                        ret[3] = createCoord(x, y - 2);
                        break;
                    case INVERT:
                        ret[0] = createCoord(x + 1 , y);
                        ret[1] = initialCoord;
                        ret[2] = createCoord(x, y - 1);
                        ret[3] = createCoord(x, y - 2);
                        break;
                    case FLIP_TWICE:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x + 1, y);
                        ret[2] = createCoord(x + 2, y);
                        ret[3] = createCoord(x + 2, y - 1);
                        break;
                    case FLIP_ONES:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x, y - 1);
                        ret[2] = createCoord(x + 1, y - 1);
                        ret[3] = createCoord(x + 2, y - 1);
                        break;

                }

                return ret;
            }
    ),
    L(
            (initialCoord, rotation) -> {
                Coord[] ret = new Coord[4];

                int x = initialCoord.getX();
                int y = initialCoord.getY();

                switch (rotation){
                    case NORMAL:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x, y - 1);
                        ret[2] = createCoord(x, y - 2);
                        ret[3] = createCoord(x + 1, y - 2);
                        break;
                    case INVERT:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x + 1, y);
                        ret[2] = createCoord(x + 1, y - 1);
                        ret[3] = createCoord(x + 1, y - 2);
                        break;
                    case FLIP_TWICE:
                        ret[0] = createCoord(x, y - 1);
                        ret[1] = createCoord(x + 1, y - 1);
                        ret[2] = createCoord(x + 2, y - 1);
                        ret[3] = createCoord(x + 2, y);
                        break;
                    case FLIP_ONES:
                        ret[0] = createCoord(x, y - 1);
                        ret[1] = initialCoord;
                        ret[2] = createCoord(x + 1, y);
                        ret[3] = createCoord(x + 2, y);
                        break;

                }

                return ret;
            }
    ),
    O(
            (initialCoord, rotation) -> {
                Coord[] ret = new Coord[4];

                int x = initialCoord.getX();
                int y = initialCoord.getY();

                ret[0] = initialCoord;
                ret[1] = createCoord(x, y - 1);
                ret[2] = createCoord(x + 1, y - 1);
                ret[3] = createCoord(x + 1, y);

                return ret;
            }
    ),
    S(
            (initialCoord, rotation) -> {
                Coord[] ret = new Coord[4];

                int x = initialCoord.getX();
                int y = initialCoord.getY();

                switch (rotation){
                    case NORMAL:
                    case INVERT:
                        ret[0] = createCoord(x , y - 1);
                        ret[1] = createCoord(x + 1 , y - 1);
                        ret[2] = createCoord(x + 1, y);
                        ret[3] = createCoord(x + 2, y);
                        break;
                    case FLIP_TWICE:
                    case FLIP_ONES:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x, y - 1);
                        ret[2] = createCoord(x + 1, y - 1);
                        ret[3] = createCoord(x + 1, y - 2);
                        break;

                }

                return ret;
            }
    ),
    Z(
            (initialCoord, rotation) -> {
                Coord[] ret = new Coord[4];

                int x = initialCoord.getX();
                int y = initialCoord.getY();

                switch (rotation){
                    case NORMAL:
                    case INVERT:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x + 1 , y);
                        ret[2] = createCoord(x + 1, y - 1);
                        ret[3] = createCoord(x + 2, y - 1);
                        break;
                    case FLIP_TWICE:
                    case FLIP_ONES:
                        ret[0] = createCoord(x + 1, y);
                        ret[1] = createCoord(x + 1, y - 1);
                        ret[2] = createCoord(x, y - 1);
                        ret[3] = createCoord(x, y - 2);
                        break;
                }

                return ret;
            }
    ),
    T(
            (initialCoord, rotation) -> {
                Coord[] ret = new Coord[4];

                int x = initialCoord.getX();
                int y = initialCoord.getY();

                switch (rotation){
                    case NORMAL:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x + 1, y);
                        ret[2] = createCoord(x + 1, y - 1);
                        ret[3] = createCoord(x + 2, y);
                        break;
                    case INVERT:
                        ret[0] = createCoord(x, y - 1);
                        ret[1] = createCoord(x + 1, y - 1);
                        ret[2] = createCoord(x + 1, y);
                        ret[3] = createCoord(x + 2, y - 1);
                        break;
                    case FLIP_TWICE:
                        ret[0] = initialCoord;
                        ret[1] = createCoord(x, y - 1);
                        ret[2] = createCoord(x + 1, y - 1);
                        ret[3] = createCoord(x, y - 2);
                        break;
                    case FLIP_ONES:
                        ret[0] = createCoord(x + 1, y);
                        ret[1] = createCoord(x + 1, y - 1);
                        ret[2] = createCoord(x, y - 1);
                        ret[3] = createCoord(x + 1, y - 2);
                        break;

                }

                return ret;
            }
    );

    private interface GenerationFigure{
        Coord[] generateFigure(Coord initialCoord, RotationMode rotation);
    }
    private GenerationFigure forms;
    CoordMask(GenerationFigure forms){
        this.forms = forms;
    }
    public Coord[] generateFigure(Coord initialCoord, RotationMode rotation){
        return this.forms.generateFigure(initialCoord, rotation);
    }

    private static Coord createCoord(int x, int y){
        return new Coord(x, y);
    }


}
