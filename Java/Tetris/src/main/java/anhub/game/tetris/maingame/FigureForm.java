package anhub.game.tetris.maingame;

import java.security.SecureRandom;

public enum FigureForm {
    I_FORM (CoordMask.I, eColor.BLUE),
    J_FORM (CoordMask.J, eColor.ORANGE),
    L_FORM (CoordMask.L, eColor.YELLOW),
    O_FORM (CoordMask.O, eColor.RED),
    S_FORM (CoordMask.S, eColor.AQUA),
    Z_FORM (CoordMask.Z, eColor.PURPLE),
    T_FORM (CoordMask.T, eColor.GREEN);
    private CoordMask mask;
    private eColor color;
    private static SecureRandom random = new SecureRandom();

    FigureForm(CoordMask mask, eColor color){
        this.mask = mask;
        this.color = color;
    }
    private static final FigureForm[] formByNumber = {I_FORM, J_FORM, L_FORM, O_FORM, S_FORM, Z_FORM, T_FORM,};
    public CoordMask getMask(){
        return this.mask;
    }
    public eColor getColor(){
        return this.color;
    }
    public static FigureForm getRandomForm() {
        int formNumber = random.nextInt(formByNumber.length);
        return formByNumber[formNumber];
    }
}
