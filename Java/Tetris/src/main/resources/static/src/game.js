var script = document.createElement('script');
script.src = 'https://code.jquery.com/jquery-3.4.1.min.js';
var cvs;
var ctx=undefined;

var orange = new Image();
orange.src="img/orange.png"
var aqua = new Image();
aqua.src="img/aqua.png"
var black = new Image();
black.src="../img/black.png"
var blue = new Image();
blue.src="../img/blue.png"
var red = new Image();
red.src="../img/red.png"
var purple = new Image();
purple.src="../img/purple.png"
var yellow = new Image();
yellow.src="../img/yellow.png"
var green = new Image();
green.src="../img/green.png"

document.addEventListener("keydown", moveUp);

function initial(){
	cvs = document.getElementById("canvas");
	ctx = cvs.getContext("2d");
	ctx.width=32*10;
	ctx.height=32*20;

}
function POST_req(direction,was_boost,was_rotation,was_esc){
	return new Promise((resolve,reject)=>{
		resolve($.ajax({
		type: 'POST',
		url: 'http://localhost:8080/step',
		data: {"direction":direction,
			   "was_boost":was_boost,
			   "was_rotation":was_rotation,
			   "was_esc": was_esc,
			   }
		}));
	})
}

var direction="AWAITING";
var was_rotation= false;
var was_boost=false;
var was_esc=false;

function check(id){

	return new Promise((resolve,reject)=>{
		resolve($.ajax({
					type: 'POST',
					url: 'http://localhost:8080/check',
					data:id
		}));
	})
}

function startGame(){
	setInterval(game_cycle, 330);

	$.ajax({
	type: 'POST',
	url: 'http://localhost:8080/step',
	data:"start"
	});

 			
}

function login(log,passw){

	return new Promise((resolve,reject)=>
	{
		resolve(
				$.ajax(
				{
					type: 'POST',
					url: 'http://localhost:8080/log',
					data:log +" " +passw
				}
					
			)
				);
	}
	)


}

		
function moveUp(event) {
 	console.log(event.key);
 	switch(event.key){
 		case "Enter":startGame();
 				break;
 		case "Escape":  was_esc=true;
 						break;
 		case "ArrowUp": was_rotation=true;
 						break;
 		case "ArrowDown":was_boost=true;
 						break;
 		case "ArrowLeft":direction="LEFT";

 						break;
 		case "ArrowRight":direction="RIGHT";
 						break;
 	}
 	console.log(direction);
}
function draw(message) {
	console.log(message);
	if(typeof ctx == 'undefined'){
		initial()

	}
	var COUNT_CELLS_X=message["field"].length;
	var COUNT_CELLS_Y=message["field"][0].length;
	console.log(COUNT_CELLS_X,COUNT_CELLS_Y);
	var CELL_SIZE=32;
	 // Какой-либо код
	 ctx.clearRect(0, 0, canvas.width, canvas.height);
	 for(let x = 0; x < COUNT_CELLS_X; x++){
	            for(let y = 0; y < COUNT_CELLS_Y; y++){
	                 var color_m = message["field"][x][y];
					 if(color_m!=null){
						ctx.drawImage(getImageColor(color_m),x*CELL_SIZE,y*CELL_SIZE);
					 }
	            }
	 }
	COUNT_CELLS_X=message["figure"].length;
	COUNT_CELLS_Y=message["figure"][0].length;
	  for(let x = 0; x < COUNT_CELLS_X; x++){
	            for(let y = 0; y < COUNT_CELLS_Y; y++){
	                 var color_f = message["figure"][x][y];
					 if(color_f!=null){
						ctx.drawImage(getImageColor(color_f),x*CELL_SIZE,y*CELL_SIZE);
					 }
	            }
 }




// Вызов функции постоянно
}

let timerId=setInterval(game_cycle, 330);

function game_cycle(){

	var temp_direct = direction;
	var temp_boost = was_boost;
	var temp_esc = was_esc;
	var temp_rot = was_rotation;
	POST_req(temp_direct,temp_boost,temp_rot,temp_esc).then(message=>
	{	
		var a=1;
		a=message["redirect"]
		console.log(a);

		if(a===null)
		{
			confirm("You are lose! Please, restart server!")
			window.location.replace("Authorization.html")}
			

	
		draw(message)
	});

	console.log(direction,was_boost,was_esc,was_rotation);
	if(temp_direct!="AWAITING") direction="AWAITING";
	if(temp_boost===true) was_boost=false;
	if(temp_rot===true) was_rotation=false;
}

function getImageColor(color){
	switch(color){
		case "black": return black;
		case "purple": return purple;
		case "orange": return orange;
		case "blue": return blue;
		case "green": return green;
		case "yellow": return yellow;
		case "aqua": return aqua;
		case "red": return red;
	 }
}

module.exports = {
						getImageColor,red,black, purple, aqua, orange,blue, green, yellow,
				 		moveUp, was_esc,was_rotation,was_boost,direction, POST_req, check, login, game_cycle
				 }