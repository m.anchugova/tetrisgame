const game = require('../src/game');



test('get color("orange") equals orange', () => {
	var a=game.getImageColor("orange");
  	expect(a).toEqual(game.orange);
  	
	});

test('get color("orange") equals orange', () => {
	var a=game.getImageColor("orange");
  	expect(a).toEqual(game.orange);
  	
	});
test('get color("black") equals black', () => {
	var a=game.getImageColor("black");
  	expect(a).toEqual(game.black);
  	
	});
test('get color("purple") equals purple', () => {
	
  		var a=game.getImageColor("purple");
  	expect(a).toEqual(game.purple);
  	
	});

test('get color("red") equals red', () => {
	var a=game.getImageColor("red");
  	expect(a).toEqual(game.red);
    	
	});
test('get color("green") equals green', () => {
	var a=game.getImageColor("green");
  	expect(a).toEqual(game.green);
  	
	});
	
test('get color("yellow") equals yellow', () => {

  	var a=game.getImageColor("yellow");
  	expect(a).toEqual(game.yellow);
  		
	});
test('get color("aqua") equals aqua', () => {
	var a=game.getImageColor("aqua");
  	expect(a).toEqual(game.aqua);
  	
	});

test('get color("blue") equals blue', () => {
		var a=game.getImageColor("blue");
  		expect(a).toEqual(game.blue);
  	
	});


test('get moveUp("Enter")', () => {
	var event = $.Event("keydown");
	event.key = "Enter";
	game.moveUp(event);
	expect(game.was_esc).toEqual(false);
	expect(game.was_rotation).toEqual(false);
	expect(game.was_boost).toEqual(false);
	expect(game.direction).toEqual("AWAITING");
});

test('get moveUp("Escape")', () => {
	var event = $.Event("keydown");
	event.key = "Escape";
	game.moveUp(event);
	expect(game.was_esc).toEqual(false);
});

test('get moveUp("ArrowUp")', () => {
	var event = $.Event("keydown");
	event.key = "ArrowUp";
	game.moveUp(event);
	expect(game.was_rotation).toEqual(false);
});


test('get moveUp("ArrowDown")', () => {
	var event = $.Event("keydown");
	event.key = "ArrowDown";
	game.moveUp(event);
	expect(game.was_boost).toEqual(false);
});


test('get moveUp("ArrowLeft")', () => {
	var event = $.Event("keydown");
	event.key = "ArrowLeft";
	game.moveUp(event);
	expect(game.direction).toEqual("AWAITING");
});


test('get moveUp("ArrowRight")', () => {
	var event = $.Event("keydown");
	event.key = "ArrowRight";
	game.moveUp(event);
	expect(game.direction).toEqual("AWAITING");
});

test('get moveUp("ArrowRight")', () => {
	var event = $.Event("keydown");
	event.key = "ArrowRight";
	game.moveUp(event);
	expect(game.direction).toEqual("AWAITING");
});

test('post_req', () => {
	var promise = game.POST_req();
	expect(promise).not.toBeNull();
});

test('ckeck id', () => {
	var promise = game.check(1);
	expect(promise).not.toBeNull();
});

test('login', () => {
	var promise = game.login("test", "test");
	expect(promise).not.toBeNull();
});


test('game cycle', () => {
	game.game_cycle();
	expect(game.direction).toEqual("AWAITING");
	expect(game.was_boost).toEqual(false);
	expect(game.was_rotation).toEqual(false);
});


  	