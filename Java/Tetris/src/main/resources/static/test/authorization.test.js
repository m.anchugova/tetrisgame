const authorization = require('../src/authorization');

const stubAwsRequest = () => ({
    promise: () => Promise.resolve($.ajax({
        type: 'POST',
        url: 'http://localhost:8080/log',
        data:log +" " +passw
    }))
});

test('login', () => {
    var promise = authorization.login("test", "test");
    expect(promise).not.toBeNull();
});

describe('login then', () => {
    it(`login sucsess`, async () => {
        const mockAwsCall = jest.fn().mockImplementation(stubAwsRequest);
        authorization.login = mockAwsCall;
        await authorization.login('test', 'test');
        expect(mockAwsCall).toBeCalledWith('test', 'test');
        expect(sessionStorage.getItem('id')).toBeNull();
        expect(window.location.URL).toBeUndefined();
    });
});
